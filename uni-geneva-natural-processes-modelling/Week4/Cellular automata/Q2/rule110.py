"""
    1D space
    discrete time

    state of a cell is a number from set S (0, 1)

    local = cell & its neighbours
    defined on neighbourhood

    ar[-1] neighbors ar[0]

"""

"""
    Ex 1. with mirrored layout, parity rule.
"""

import numpy as np


# def check_table_correct_mirrored(dim_1_arr):
#     y = np.array(dim_1_arr)
#     x = y.reshape(4, 4)
#
#     z = np.zeros((6, 6))
#
#     # insert in center
#     for i in range(1, 5):
#         for j in range(1, 5):
#             z[i][j] = x[i-1][j-1]
#
#     for i in range(5):
#         z[0][i] = z[-2][i]
#         z[-1][i] = z[1][i]
#         z[i][-1] = z[i][1]
#         z[i][0] = z[i][-2]
#
#     print("------")
#     print(z)
#
#     correct = True
#
#     for i in range(1, 5):
#         for j in range(1, 5):
#             sum = z[i-1][j] + z[i][j-1] + z[i+1][j] + z[i, j+1]
#             val = sum % 2
#             if z[i][j] != val:
#                 correct = False
#                 break
#
#     print(correct)
#     print("===")

def simulate(array, steps=1):
    """
        Single initial value accepted. (all zeros and single 1)
    """

    matrix_view = list()
    matrix_view.append(array)

    iterations = 0

    while (iterations < steps):

        new_array = [0 for k in array]

        # generate new array
        for i in range(1, len(array) - 1):
            check_vec = array[i-1:i+2]
            if check_vec == [0, 0, 0]:
                new_array[i] = 0
            elif check_vec == [0, 0, 1]:
                new_array[i] = 1
            elif check_vec == [0, 1, 0]:
                new_array[i] = 1
            elif check_vec == [0, 1, 1]:
                new_array[i] = 1
            elif check_vec == [1, 0, 0]:
                new_array[i] = 0
            elif check_vec == [1, 0, 1]:
                new_array[i] = 1
            elif check_vec == [1, 1, 0]:
                new_array[i] = 1
            elif check_vec == [1, 1, 1]:
                new_array[i] = 0
        # end new array

        # append new array to matrix_view
        matrix_view.append(new_array)
        array = new_array


        iterations += 1

    for line in matrix_view:
        print(line)

array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
steps = 8

simulate(array, steps)
