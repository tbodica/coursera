# -*- coding: utf-8 -*-
# Copyright (C) 2016 Universite de Geneve, Switzerland
# E-mail contact: sebastien.leclaire@etu.unige.ch
#
# The Parity Rule
#

from numpy import *
import matplotlib.pyplot as plt
from matplotlib import cm
import scipy
import scipy.misc
import copy

# Definition of functions
def readImage(string): # This function only work for monochrome BMP.
    image =  scipy.misc.imread(string,1);
    image[image == 255] = 1
    image = image.astype(int)
    return image # Note that the image output is a numPy array of type "int".

def countWhitePixels(image):
    sum = 0
    for line in image:
        for i in line:
            sum += i
    return sum

# Main Program

# Program input, i.e. the name of the image "imageName" and the maximum number of iteration "maxIter"
imageName = 'image3.bmp'
maxIter   = 32

# Read the image and store it in the array "image"
image = readImage(imageName) # Note that "image" is a numPy array of type "int".
# Its element are obtained as image[i,j]
# Also, in the array "image" a white pixel correspond to an entry of 1 and a black pixel to an entry of 0.

# Get the shape of the image , i.e. the number of pixels horizontally and vertically.
# Note that the function shape return a type "tuple" (vertical_size,horizontal_size)
imageSize = shape(image);

# Print to screen the initial image.
print('Initial image:')
plt.clf()
plt.imshow(image, cmap=cm.gray)
plt.show()
plt.pause(0.1)

# Main loop
for it in range(1,maxIter+1):

    imageCopy = copy.copy(image);

    # You need to write here the core of the parity rule algorithm.
    # Altough not mandatory, you might need to use the array "imageCopy" and the tuple "imageSize".

    # print("image before current iteration")
    # plt.clf()
    # plt.imshow(image, cmap=cm.gray)
    # plt.show()
    # plt.pause(0.1)

    # print(len(image))
    # print(image)

    # iterate image1
    for i in range(len(image)):
        for j in range(len(image[i])):
            #print(image[i][j])

            # find neighbours
            n_above = 0
            n_below = 0
            n_right = 0
            n_left = 0
            if i - 1 < 0:
                n_above = image[-1][j]
            else:
                n_above = image[i-1][j]

            if i + 1 >= len(image) - 1:
                n_below = image[0][j]
            else:
                n_below = image[i+1][j]

            if j + 1 >= len(image[i]) - 1:
                n_right = image[i][0]
            else:
                n_right = image[i][j+1]

            if j - 1 < 0:
                n_left = image[i][-1]
            else:
                n_left = image[i][j-1]
            # end find neighbours

            # apply neighbour logic
            val = (n_right + n_left + n_above + n_below) % 2
            imageCopy[i][j] = val

    # end iterate image1


    image = imageCopy
    #if it == 32:
    # Print to screen the image after each iteration.
    print('Image after',it,'iterations:')
    plt.clf()
    plt.imshow(image, cmap=cm.gray)
    plt.show()
    plt.pause(0.1)

    print("wp: {0}".format( countWhitePixels(image) ))

# Print to screen the number of white pixels in the final image
print("The number of white pixels after",it,"iterations is: ", sum(image))
