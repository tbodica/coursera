"""
    1D space
    discrete time

    state of a cell is a number from set S (0, 1)

    local = cell & its neighbours
    defined on neighbourhood

    ar[-1] neighbors ar[0]

"""

"""
    Ex 1. with zeros layout, custom rule.
"""

import numpy as np


def check_table_correct_mirrored(dim_1_arr):
    y = np.array(dim_1_arr)
    x = y.reshape(4, 4)

    z = np.zeros((6, 6))

    # insert in center
    for i in range(1, 5):
        for j in range(1, 5):
            z[i][j] = x[i-1][j-1]

    print("------")
    print(z)

    correct = True

    for i in range(1, 5):
        for j in range(1, 5):
            sum = z[i-1][j] + 2*z[i+1][j] + 4*z[i][j-1] + 8*z[i, j+1]
            val = sum % 2
            if z[i][j] != val:
                correct = False
                break

    print(correct)
    print("===")


#check_table_correct(range(1, 17))
check_table_correct_mirrored([0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1])
check_table_correct_mirrored([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])
check_table_correct_mirrored([1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1])
check_table_correct_mirrored([0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0])
