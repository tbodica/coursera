import sys

class SchemeSolver:
    def __init__(self):
        self.t0 = 0
        self.tf = 1.5
        self.s0 = 1

    def explicit(self, dt, iter):
        if iter != 0:
            return self.explicit(dt, iter-1) * (1 - 10*dt)
        else:
            return self.s0

    def implicit(self, dt, iter):
        if iter != 0:
            return self.implicit(dt, iter-1) / (1 + 10*dt)
        else:
            return self.s0


args = sys.argv
#print(args)
dtv = args[1]
iters = args[2]

o = SchemeSolver()
val = o.explicit(dt = float(dtv), iter = int(iters))
val2 = o.implicit(dt = float(dtv), iter = int(iters))
print("explicit: ")
print(val)
print("implicit")
print(val2)
