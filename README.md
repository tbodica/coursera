# Coursera Projects

Currently, the repositories for the follwoing courses are attached:

- Machine Learning (Andrew NG), Coursera
- IBM Data Science Professional Certificate
- Simulation and modelling of natural processes, University of Geneva

# IBM Data Science

Contains use of geospatial libraries in python, as well as techniques for manipulating data, such as Pandas. The key is the theoretical content about data science, data engineering, the data science lifecycle from business modelling to continuous integration.