# Week 1

## Model representation
A dataset with prices for houses in Portland, OR.
A house of size in feet^2 is mapped to a price. It is possible to plot this on a 2d scatter.

### Regression problem
A supervised learning solution can be used to draw a line for predicting prices. (Note 1 Video: Model representation).

m -> number of training examples
x -> input variable (features)
y -> output variable (target variable to predict)

## Cost function
The accuracy of the hypothesis is measured using a **cost function**. See Reading: Cost function

J(theta0, theta1) = 1/(2m) sum from 1 to m of ( Ypred - Y )^2 = 1/(2m) sum 1->m (hTheta(Xi) - Yi)^2

hTheta(Xi) is the hypothesis function's output

## Gradient descent
An algorithm that can be used for optimizing the cost function J (in linear regression).