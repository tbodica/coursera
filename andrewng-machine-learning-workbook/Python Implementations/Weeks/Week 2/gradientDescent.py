
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from math import pow


o = pd.read_csv('Datasets/ex1data1.txt')

xIn = o['x']
yIn = o['y']

X = np.insert(np.matrix(xIn.values, ), 0, 1, axis=0).transpose() # type numpy.matrix
y = np.matrix(yIn.values)

theta = np.matrix([0, 0]).transpose() # column vector 0, 0

iterations = 1500
alpha = 0.01


J = None # cost function


def computeCost(X, y, theta):
    m = y.shape[1]
    J = 0

    t_sum = 0

    index = 0
    for _i in X:
        as_array = _i.A[0]
        x0 = theta.A[0]
        x1 = as_array[1] * theta.A[1]

        hTheta = x0 + x1
        difference = hTheta - y.A[0][index]
        index += 1

        t_sum += pow(difference, 2)

    J = (1/(2*float(m))) * (t_sum)
    return J

print("Testing the cost function...")
print(computeCost(X, y, theta))

