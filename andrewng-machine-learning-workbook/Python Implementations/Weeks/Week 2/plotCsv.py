import matplotlib.pyplot as plt
import pandas as pd

o = pd.read_csv('Datasets/ex1data1.txt')

X = o['x']
y = o['y']

plt.scatter(X, y)
plt.show()
