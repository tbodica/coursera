# Linear Regression with Multiple Variables

An analysis of programming assignment.

## 1. Plotting the data
A .csv is given like:

6.1101,17.592
5.5277,9.1302
8.5186,13.662

```
X = data(:, 1); y = data(:, 2); // this means load the data, X is column 1, y is column 2
m = length(y); % number of training examples // len(y) is examples number, so should len(x)

% Plot Data
% Note: You have to complete the code in plotData.m
plotData(X, y);
```
![Octave](Images/plottedData.png)
![Matplotlib](Images/mpl.png)

## 2. Cost and gradient descent

First, a column of ones is inserted into X, which contains the first column of the input. Then theta is initialized with a column of two zeros.

We assume that there are 1500 iterations, with 0.01 alpha for gradient descent. J is computed and displayed.

The cost function is a squared error function. The python code should produce the same results as the octave code.

![CostFunction](Images/cost_function.png)
