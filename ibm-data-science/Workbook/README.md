## Data Science Workbook
Compilation of exercises and data science from a coursera course, IBM Data Science Specialization.

### Chapters

Course 5, Cloud object storage interaction snippets


    Libraries:
    - boto3
    - bokeh

Course 6, IBM_DB connection, SQL, IPython, Jupyter


    Libraries:
    - ibm_db
    - ipython-sql
    - seaborn
    - pandas


    Datasets:
    - Chicago public data
    - Chicago schools

Course 8, Machine Learning With Python


    Libraries:
    - numpy
    - matplotlib
    - sklearn


    Topics:
    - Pearson Correlation
    - DBSCAN
    - Decision Trees
    - Preprocessing Data
    - Model Evaluation
    - Hierarchical Clustering, Aglomerative Clustering, Dendrograms
    - Clustering, K-Means
    - Data Visualization
    - Regression


    Datasets:
    - Movies Dataset from grouplens.org/datasets
    - Canada demographics and fuel consumption, CO2 Emissions

Course 9, Data Science Capstone
    
    Case study, Foursquare API, Folium.