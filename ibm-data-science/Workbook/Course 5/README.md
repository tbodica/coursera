In the assignment, much of the code will be provided. When you call the function make_dashboard the function will create and save a file index.html; This file contains the dashboard, you must place this file in IBM object storage, you can then view the dashboard via a URL. You will need the variable file_name.

file_name= "index.html"

There is option to view the credentials you have created. Click View Credentials, you will get credentials in the format given below. Copy your Credentials and assign them to the variable credentials given in the notebook in Question 5 #Hidden cell. Format for credentials as follow:

credentials = {
  "apikey": "your-api-key",
  "cos_hmac_keys": {
  "access_key_id": "your-access-key-here",
  "secret_access_key": "your-secret-access-key-here"
  },
 "endpoints": "your-endpoints",
  "iam_apikey_description": "your-iam_apikey_description",
  "iam_apikey_name": "your-iam_apikey_name",
  "iam_role_crn": "your-iam_apikey_name",
  "iam_serviceid_crn": "your-iam_serviceid_crn",
 "resource_instance_id": "your-resource_instance_id"
}

There will be several other steps, but don't worry all the instructions will be provided in the lab.