# Machine Learning with Python

### Techniques
Regression and Estimation
Classification
Clustering
Prediction

### Tools
1. NumPy
2. SciPy
3. Matplotlib
4. Pandas
5. Sci-Kit Learn

### About#

The usual pipeline for machine learning is: Preprocessing -> Training/Testing split of the data -> Algorithm setup -> Fitting -> Prediction -> Evaluation -> Exporting

Content based filtering in this course is presented with an example of movie ratings and user preference and recommendations, correlations with the pearson correlation coefficient for example: https://en.wikipedia.org/wiki/Pearson_correlation_coefficient