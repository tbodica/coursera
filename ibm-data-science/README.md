# Introduction
Having access to free public data from official and specialized providers such as e-estonia, Estonian Weather Service, tallinn.ee, stat.ee, and third party data platforms such as Kaggle and Foursquare, the objective is to gain understanding about underdeveloped regions where the demand calculated from factors such as a region's population and area could support new businesses.

# Data
Estonia has easily accessible data, from providers such as e-estonia, Estonian Weather Service, tallinn.ee, stat.ee. A variety of lists that use this data can be found in table form in wikipedia, for example lists containing data about: weather, districts, construction, population, economy, etc.

The data is easy to embed in a Jupyter notebook using the pandas library and read_html or read_csv.

We are interested in small to medium businesses such as coffee shops and restaurants, small offices.

# Methodology
In the Jupyter notebook, we first download the datasets, then after seeing the shapes of the data, we plot the values that we'd like to see graphically.

Correlations are not necessary for the level of depth chosen for this discussion, however one could aggregate all data into a single dataset, and use `pandas` or `sql` as an interface. 

Graphical visualization includes area plots, histograms, and `Folium` maps. 

After seeing the most populated districts and what prices are usually found for restaurants, we can see geographically how they are distributed.

# Results
The distribution of the population inside the districts of Tallinn is clearly visible, the sudden price drop point between large businesses in dense areas and small businesses in less populated areas is visible, from the fourth most populated district downward.

# Discussion
The level of depth chosen for the ammount of time dedicated to this project can only be shallow. Deeper correlations, thousands of API calls to Foursquare API, and clustering or machine learning analysis can be performed with less restrictions, to see better correlations, however it was not necessary for this level of accuracy, especially since the data shows population drops between districts of up to 15000 inhabitants.

# Conclusion
A good starting place has been found for investors wanting a small to medium size business.

